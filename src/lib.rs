// Captures the forms of a command line argument
//
// Examples of a form would be `-o`, `--output`, `-output`, `+N` and so on.
// This structure is intended for the exchange of data between programs
// that need to handle argument names (e.g. command line parsers and
// documentation generators).
#[derive(Debug)]
pub struct BaseArg<T>
where
    T: AsRef<str> + Clone,
{
    pub forms: Vec<T>,
}

